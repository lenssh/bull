/**
 * 1. 默认值只需要实现 Default trait 即可
#[derive(Default)]
struct AppState {
db: HashMap<String, Bytes>
}

2. axum ShareState
let shared_state = SharedState::default();
let app = Router::with_state(Arc::clone(&shared_state)). route()

3. return Result ？操作失败问题
impl From for YouError

4 Result中的Option问题
    let ops = abc_option_b().zip( abc_option_a() );
    if  let Some((a,b)) = ops  {

        let ra = abc_result_a()? ;
        let rb = abc_result_b()?;
    
        println!("a = {}, b={}, ra = {}, rb = {}", a, b, ra, rb);
        Ok(())
    } else {
        Err( common::Error::UnknownIndex("err".to_string()) )
    }  
5. Async/.await
6. tracing
 * 
 */
use common;
use std::result::Result;

pub type Gresult<T> = std::result::Result<T, common::Error>;

#[test]
fn abc_works() {
    let result = common::add(2, 2);
    assert_eq!(result, 4);
}
