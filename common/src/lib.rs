mod error;
use serde::{Deserialize, Serialize};
use std::time::SystemTime;

pub use error::Error;

#[derive(Serialize, Deserialize, Debug)]
pub struct SearchResult {
    q: String,
    page: u32,
    hits: Vec<SearchDoc>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct SearchDoc {
    pub docid: String,
    pub author: String,
    pub tags: String,
    pub fname: String,
    pub content: String,
    pub score: f32,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct Params {
    page: Option<i32>,
    q: Option<String>,
}

pub trait Iss {
    fn get_writer();
    fn get_reader();
}

pub fn add(left: usize, right: usize) -> usize {
    let timespec = SystemTime::now();
    println!("{:#?}", timespec);
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
