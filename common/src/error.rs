//! This contains Toshi's server errors, if you are looking for the hyper or tonic errors they are
//! located in the [`crate::extra_errors`]: extra_errors
//! module

use std::fmt::Debug;
// use std::iter::from_generator;

use serde::{Deserialize, Serialize};
use thiserror::Error;

use tantivy::query::QueryParserError;
use tantivy::TantivyError;

/// Toshi's base error types
#[derive(Debug, Error)]
pub enum Error {
    /// IO error that deals with anything related to reading from disk or network communications
    //#[error("Int Error: {0}")]
    //IntError(#[from] i32),
    /// A query that had a syntax error or was otherwise not valid
    //#[error("Error in here: '{0}'")]
    //StrError(#[from] String),
    /// This should never occur and is a bug that should be reported
    #[error("An unknown error occurred")]
    UnknownError(),
    #[error("get data is error!")]
    GetError(),
    #[error("read data {0} error")]
    AlreadyExists(String),
    #[error("Error from: '{0}'. translate for Result op ?")]
    Err4Result(String),
    #[error("Error in Index: '{0}'")]
    TantivyError(#[from] anyhow::Error),
    #[error("Error in Index: '{0}'")]
    IoError(#[from] std::io::Error),
    // When an invalid log config is provided
    //    #[error("Error Deserializing Error: '{0}'")]
    //    TomlError(toml::de::Error),
}

impl From<TantivyError> for Error {
    fn from(err: TantivyError) -> Self {
        Error::TantivyError(err.into())
    }
}
impl From<QueryParserError> for Error {
    fn from(err: QueryParserError) -> Self {
        Error::TantivyError(err.into())
    }
}

/*

impl From<T> for Error {
    fn from(err:T) -> Self {
        Error::T( err.into())
    }
}


impl From<&str> for Error {
    fn from(err:&str) -> Self {
        Error::StrError(err.into())
    }
}
*/

/*
// Make our own error that wraps `anyhow::Error`.
pub struct AppError(anyhow::Error);

// This enables using `?` on functions that return `Result<_, anyhow::Error>` to turn them into
// `Result<_, AppError>`. That way you don't need to do that manually.
impl<E> From <E> for AppError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}
*/
