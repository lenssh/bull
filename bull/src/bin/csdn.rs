extern crate utils;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    // println!("{:?}", args);
    if args.len() < 2 {
        println!("{} url", args[0]);
        return;
    }
    if let Some(e) = utils::get_csdn(&args[1]).err() {
        println!("error: {:#?}", e);
    }
}
